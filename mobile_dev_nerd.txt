"Mobile Dev Nerd" by Maciej G�rski

He doesn't have a problem with perl
He just doesn't use it
He's fine that his mates have SVNs
But he thinks they'll regret it
He likes doing reviews
But he hates when the code's too smelly
He tends not to go to dev conferences
Cos he can't stand the crowd
But all he's ever wanted to be
Is a Scrum Master in Google or SoftwareMill
But he knows that it's not very likely
He'll soon turn thirty

He knows that he will always be
A mobile dev nerd
He'll keep writing code the world will never see
And though it won't be seen
He'll just keep coding
Oh yeah...

original: http://www.youtube.com/watch?v=XP9pnSXhibw
